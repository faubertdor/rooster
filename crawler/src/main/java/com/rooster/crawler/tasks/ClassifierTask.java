package com.rooster.crawler.tasks;

import com.google.inject.Inject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import akka.NotUsed;
import akka.stream.javadsl.Flow;
import com.rooster.core.BalancerFactory;
import com.rooster.core.Task;
import com.rooster.crawler.web.Page;

public class ClassifierTask implements Task<Page, Page> {
  private static final Logger logger = LoggerFactory.getLogger(ClassifierTask.class);

  @Inject
  public ClassifierTask() {}

  @Override
  public Page execute(Page page) {

    System.out.println("<--------------------------------------------------------------------------------------------------------------------------------------------->");
    System.out.println("<-------------------------------------------------------CLASSIFIER----------------------------------------------------------------->");
    System.out.println("<--------------------------------------------------------------------------------------------------------------------------------------------->");
    System.out.println(String.format("URL: %s - Page Title: %s", page.getUrl(), page.getTitle()));
    System.out.println("<--------------------------------------------------------------------------------------------------------------------------------------------->");

    return page;
  }

  @Override
  public Flow<Page, Page, NotUsed> flow(int parallelism) {
    return BalancerFactory.balancer(Flow.of(Page.class).map(this::execute), parallelism);
  }
}
