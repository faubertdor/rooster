package com.rooster.crawler.tasks;

import com.google.inject.Inject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import akka.NotUsed;
import akka.stream.javadsl.Flow;
import com.rooster.core.BalancerFactory;
import com.rooster.core.Task;
import com.rooster.crawler.web.Page;
import org.jsoup.nodes.Document;

public class DataScraperTask implements Task<Document, Page> {
  private static final Logger logger = LoggerFactory.getLogger(DataScraperTask.class);

  @Inject
  public DataScraperTask() {}

  @Override
  public Page execute(Document document) {
    Page page = new Page(document.baseUri(), document.title(), document.text());

    System.out.println("<--------------------------------------------------------------------------------------------------------------------------------------------->");
    System.out.println("<-------------------------------------------------------DATA SCRAPING----------------------------------------------------------------->");
    System.out.println("<--------------------------------------------------------------------------------------------------------------------------------------------->");
    System.out.println(String.format("URL: %s - Page Title: %s", page.getUrl(), page.getTitle()));
    System.out.println("<--------------------------------------------------------------------------------------------------------------------------------------------->");

    return page;
  }

  @Override
  public Flow<Document, Page, NotUsed> flow(int parallelism) {
    return BalancerFactory.balancer(Flow.of(Document.class).map(this::execute), parallelism);
  }
}
