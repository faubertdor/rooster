package com.rooster.crawler.tasks;

import com.google.inject.Inject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import akka.NotUsed;
import akka.actor.ActorRef;
import akka.stream.javadsl.Flow;
import com.rooster.core.BalancerFactory;
import com.rooster.core.Task;
import com.rooster.crawler.web.crawl.SchedulerActorRef;
import com.rooster.crawler.web.UrlContext;
import com.rooster.crawler.web.PageContext;
import com.rooster.crawler.web.UrlRepository;
import crawlercommons.robots.SimpleRobotRules;
import org.glassfish.jersey.client.JerseyClient;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import javax.ws.rs.ProcessingException;
import javax.ws.rs.WebApplicationException;

public class PageDownloaderTask implements Task<UrlContext, Document> {
  private static final String LINKS_SELECTOR = "a[href]";
  private static final Logger logger = LoggerFactory.getLogger(PageDownloaderTask.class);
  private ActorRef scheduler;
  private JerseyClient client;
  private UrlRepository repository;

  @Inject
  public PageDownloaderTask(@SchedulerActorRef ActorRef scheduler, JerseyClient client, UrlRepository repository) {
    this.scheduler = scheduler;
    this.client = client;
    this.repository = repository;
  }

  @Override
  public Document execute(UrlContext context) {
    Document document = null;

    if (context == null || context.getOrigin() == null || context.getUrl() == null) {
      throw new RuntimeException("Invalid link context");
    }

    SimpleRobotRules rules = context.getOrigin().getRules();
    String url = context.getUrl();
    String wwwRootUrl = context.getOrigin().wwwRootUrl();

    System.out.println("Access: " + (rules.isAllowed(url) ? "GRANTED" : "DENIED"));
    if (repository.getVisited(wwwRootUrl).contains(url) || !rules.isAllowed(url)) { // TODO: Add this check in the scheduler
      throw new RuntimeException("Access denied or we already visited this link");
    }

    try {
      String page = client.target(url)
          .request()
          .get(String.class);

      repository.addVisited(context);
      if (page != null && !page.isEmpty()) {
        document = Jsoup.parse(page);
        document.setBaseUri(url);
        scheduler.tell(new PageContext(context.getOrigin(), document.select(LINKS_SELECTOR)), ActorRef.noSender());


        System.out.println("<--------------------------------------------------------------------------------------------------------------------------------------------->");
        System.out.println("<-------------------------------------------------------Page Downloader----------------------------------------------------------------->");
        System.out.println("<--------------------------------------------------------------------------------------------------------------------------------------------->");
        System.out.println(String.format("URL: %s - Page Title: %s", context.getUrl(), document.title()));
        System.out.println("<--------------------------------------------------------------------------------------------------------------------------------------------->");
      }
    } catch (ProcessingException | WebApplicationException e) {
      logger.error("Failed to execute http request", e);
      throw new RuntimeException(e);
    } catch (Exception e) {
      e.printStackTrace();
      logger.error("Failed to download web page", e);
      throw new RuntimeException(e);
    }

    return document;
  }

  @Override
  public Flow<UrlContext, Document, NotUsed> flow(int parallelism) {
    return BalancerFactory.balancer(Flow.of(UrlContext.class).map(this::execute), parallelism);
  }
}
