package com.rooster.crawler;

import com.google.inject.Inject;

import akka.Done;
import akka.actor.ActorRef;
import akka.stream.ActorMaterializer;
import akka.stream.OverflowStrategy;
import akka.stream.javadsl.Sink;
import akka.stream.javadsl.Source;
import akka.stream.javadsl.SourceQueueWithComplete;
import com.rooster.crawler.tasks.ClassifierTask;
import com.rooster.crawler.tasks.DataScraperTask;
import com.rooster.crawler.tasks.PageDownloaderTask;
import com.rooster.crawler.web.UrlContext;
import com.rooster.crawler.web.Page;
import com.rooster.crawler.web.crawl.SchedulerActor.Init;
import com.rooster.crawler.web.crawl.SchedulerActorRef;

import java.time.Duration;
import java.util.concurrent.CompletionStage;

public class StreamTopology {

  @Inject
  private ActorMaterializer materializer;

  @Inject
  private CrawlerConfiguration config;

  @Inject
  @SchedulerActorRef
  private ActorRef scheduler;

  @Inject
  private PageDownloaderTask downloader;

  @Inject
  private DataScraperTask scraper;

  @Inject
  private ClassifierTask classifier;

  public void run() {
    final Sink<Page, CompletionStage<Done>> sink = Sink.foreach(
        x -> {
          System.out.println("<-------------------------------------------------------****SINK***----------------------------------------------------------------->");
          System.out.println("URL: " + x.getUrl());
          System.out.println("Title: " + x.getTitle());
        }
    );

    int bufferSize = 100;
    int elementsToProcess = 20;
    SourceQueueWithComplete<UrlContext> source = Source.<UrlContext>queue(bufferSize, OverflowStrategy.backpressure())
        .throttle(elementsToProcess, Duration.ofSeconds(3))
        .via(downloader.flow(10))
        .via(scraper.flow(10))
        .via(classifier.flow(10))
        .to(sink)
        .run(materializer);

    scheduler.tell(new Init(source), ActorRef.noSender());
  }
}
