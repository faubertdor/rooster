package com.rooster.crawler;

import com.fasterxml.jackson.annotation.JsonProperty;

import com.rooster.core.config.ApplicationConfiguration;
import com.rooster.crawler.web.crawl.SchedulerConfiguration;

import javax.validation.constraints.NotNull;

public class CrawlerConfiguration implements ApplicationConfiguration {

  @NotNull
  @JsonProperty
  private String applicationName;

  @NotNull
  @JsonProperty
  private SchedulerConfiguration schedulerConfiguration;

  @Override
  public String getApplicationName() {
    return applicationName;
  }

  public SchedulerConfiguration getSchedulerConfiguration() {
    return schedulerConfiguration;
  }
}
