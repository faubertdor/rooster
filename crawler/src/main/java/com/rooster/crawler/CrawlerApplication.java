package com.rooster.crawler;

import com.google.inject.Guice;
import com.google.inject.Injector;

import com.rooster.core.Utils;
import com.rooster.crawler.di.ApplicationGuiceModule;


public class CrawlerApplication {
  private CrawlerConfiguration config;

  /**
   * Main entry point for the crawler application
   *
   * @param args App config args
   */
  public static void main(String[] args) {
    new CrawlerApplication().run(args);
  }

  /**
   * Parse config args then run the app
   *
   * @param args App config args
   */
  private void run(String[] args) {
    initConfig(args);

    Injector bootstrapInjector = Guice.createInjector(new ApplicationGuiceModule(config));
    StreamTopology topology = bootstrapInjector.getInstance(StreamTopology.class);
    topology.run();
  }

  /**
   * Parses the config arg then set config
   *
   * @param args App config args
   */
  private void initConfig(String[] args) {
    if (args == null || args.length == 0) {
      throw new RuntimeException("config.yaml arg is required to run application");
    }

    this.config = Utils.readYamlConfig(args[0], CrawlerConfiguration.class);
  }
}
