package com.rooster.crawler.di;

import com.google.inject.AbstractModule;
import com.google.inject.Provides;
import com.google.inject.Singleton;

import akka.actor.ActorRef;
import akka.actor.ActorSystem;
import akka.stream.ActorMaterializer;
import akka.stream.ActorMaterializerSettings;
import com.rooster.core.StreamSupervision;
import com.rooster.crawler.CrawlerConfiguration;
import com.rooster.crawler.web.UrlRepository;
import com.rooster.crawler.web.crawl.RobotRulesParser;
import com.rooster.crawler.web.crawl.SchedulerActor;
import com.rooster.crawler.web.crawl.SchedulerActorRef;
import crawlercommons.robots.SimpleRobotRulesParser;
import org.glassfish.jersey.client.JerseyClient;
import org.glassfish.jersey.client.JerseyClientBuilder;

import java.net.URI;
import java.net.URISyntaxException;

public class ApplicationGuiceModule extends AbstractModule {
  private CrawlerConfiguration config;

  public ApplicationGuiceModule(CrawlerConfiguration config) {
    this.config = config;
  }

  @Override
  protected void configure() {
    bind(SimpleRobotRulesParser.class).toInstance(new SimpleRobotRulesParser());
    bind(JerseyClient.class).toInstance(new JerseyClientBuilder().build());
    bind(UrlRepository.class).toInstance(new UrlRepository());
  }

  @Provides
  @Singleton
  ActorSystem providesActorSystem() {
    return ActorSystem.create();
  }

  @Provides
  @Singleton
  ActorMaterializer providesActorMaterializer(ActorSystem system) {
    return ActorMaterializer.create(
        ActorMaterializerSettings.create(system)
            .withSupervisionStrategy(new StreamSupervision()),
        system
    );
  }

  @Provides
  @Singleton
  @SchedulerActorRef
  ActorRef providesSchedulerActorRef(
      ActorSystem system,
      JerseyClient client,
      RobotRulesParser robotstxt,
      UrlRepository repository
  ) {
    return system.actorOf(
        SchedulerActor.getProps(
            config.getSchedulerConfiguration(),
            client,
            robotstxt,
            repository
        ),
        SchedulerActorRef.class.getSimpleName()
    );
  }

  @Provides
  @Singleton
  RobotRulesParser providesRobotRulesParser(SimpleRobotRulesParser parser) {

    try {
      return new RobotRulesParser(new URI("http://example.com"), "kokoriko", parser);
    } catch (URISyntaxException e) {
      throw new RuntimeException(e);
    }
  }
}
