package com.rooster.crawler.web.crawl;

import org.apache.commons.lang3.StringUtils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import akka.actor.AbstractActor;
import akka.actor.Props;
import akka.stream.javadsl.SourceQueueWithComplete;
import com.rooster.crawler.web.PageContext;
import com.rooster.crawler.web.Site;
import com.rooster.crawler.web.UrlContext;
import com.rooster.crawler.web.UrlRepository;
import org.glassfish.jersey.client.JerseyClient;

import java.net.URI;
import java.net.URISyntaxException;

public class SchedulerActor extends AbstractActor {
  private static final Logger logger = LoggerFactory.getLogger(SchedulerActor.class);
  private static final String ROBOTS_TXT_PATH = "/robots.txt";
  private static final String ABSOLUTE_HREF = "abs:href";
  private static final String RELATIVE_HREF = "href";
  private SchedulerConfiguration config;
  private JerseyClient client;
  private RobotRulesParser robotstxt;
  private SourceQueueWithComplete<UrlContext> source;
  private UrlRepository repository;

  private SchedulerActor(
      SchedulerConfiguration config,
      JerseyClient client,
      RobotRulesParser robotstxt,
      UrlRepository repository
  ) {
    this.config = config;
    this.client = client;
    this.robotstxt = robotstxt;
    this.repository = repository;
  }

  public static Props getProps(
      SchedulerConfiguration config,
      JerseyClient client,
      RobotRulesParser robotstxt,
      UrlRepository repository
  ) {
    return Props.create(
        SchedulerActor.class,
        () ->
            new SchedulerActor(
                config,
                client,
                robotstxt,
                repository
            )
    );
  }

  @Override
  public Receive createReceive() {
    return receiveBuilder()
        .match(Init.class, this::init)
        .match(PageContext.class, this::execute)
        .build();
  }

  /**
   * When receive an init message this actor set its state with the stream source
   *
   * @param init Init message
   */
  private void init(Init init) {
    if (this.source == null) {
      this.source = init.getSource();


      config.getFrontier()
          .parallelStream()
          .forEach(url -> {
            try {
              Site site = new Site(new URI(url), robotstxt.parse(client.target(url + ROBOTS_TXT_PATH).request().get(String.class)));
              source.offer(new UrlContext(site, site.getUri().toASCIIString()));
            } catch (URISyntaxException e) {
              logger.error(String.format("Failed to parse URL - %s", url), e);
            }
          });
    }
  }

  private void execute(PageContext context) {
    if (context == null || context.getOrigin() == null || context.getLinks() == null) {
      return;
    }

    context.getLinks()
        .parallelStream()
        .forEach(
            link -> {
              String rootUrl = context.getOrigin().getRootUrl();
              String wwwRootUrl = context.getOrigin().wwwRootUrl();
              String newLink = link.attr(ABSOLUTE_HREF);

              // If link is relative concat with base URL
              if (StringUtils.isBlank(newLink)) {
                newLink = link.attr(RELATIVE_HREF);
                newLink = rootUrl + newLink;
              }

              // Eliminate anchor link
              String[] parts = newLink.split("#");
              newLink = parts[0].trim().toLowerCase();


              // If valid and newly discovered the recursively visit the page
              if ((newLink.startsWith(rootUrl) || newLink.startsWith(wwwRootUrl)) && !repository.getDiscovered(wwwRootUrl).contains(newLink)) {
                System.out.println("Website: " + wwwRootUrl + " - Discovered total: " + repository.getDiscovered(wwwRootUrl).size());
                System.out.println("Website: " + wwwRootUrl + " - Visited total: " + repository.getVisited(wwwRootUrl).size());

                repository.addDiscovered(new UrlContext(context.getOrigin(), newLink));
                source.offer(new UrlContext(context.getOrigin(), newLink));
              }
            }
        );
  }

  public static class Init {
    private SourceQueueWithComplete<UrlContext> source;

    public Init(SourceQueueWithComplete<UrlContext> source) {
      this.source = source;
    }

    public SourceQueueWithComplete<UrlContext> getSource() {
      return source;
    }
  }
}
