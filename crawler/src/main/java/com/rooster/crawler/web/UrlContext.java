package com.rooster.crawler.web;

import com.google.common.base.Preconditions;

import java.util.Objects;

public class UrlContext {
  private Site origin;
  private String url;

  public UrlContext(Site origin, String url) {
    Preconditions.checkNotNull(origin, "site cannot be null");
    Preconditions.checkNotNull(url, "url cannot be null");

    this.origin = origin;
    this.url = url;
  }

  public Site getOrigin() {
    return origin;
  }

  public String getUrl() {
    return url;
  }

  @Override
  public boolean equals(Object object) {
    if (this == object) {
      return true;
    }

    if (!(object instanceof UrlContext)) {
      return false;
    }

    UrlContext that = (UrlContext) object;
    return Objects.equals(origin, that.origin) &&
        Objects.equals(url, that.url);
  }

  @Override
  public int hashCode() {
    return Objects.hash(origin, url);
  }
}
