package com.rooster.crawler.web;

import com.google.inject.Inject;
import com.google.inject.Singleton;

import org.apache.commons.lang3.StringUtils;

import org.glassfish.jersey.internal.guava.Sets;

import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

@Singleton
public class UrlRepository {
  private ConcurrentMap<String, Set<String>> discovered;
  private ConcurrentMap<String, Set<String>> visited;

  @Inject
  public UrlRepository() {
    this.discovered = new ConcurrentHashMap<>();
    this.visited = new ConcurrentHashMap<>();
  }

  public void addDiscovered(UrlContext context) {
    if (context != null) {
      String wwwRootUrl = context.getOrigin().wwwRootUrl();
      Set<String> urls = discovered.getOrDefault(context.getOrigin().wwwRootUrl(), Sets.newHashSet());
      urls.add(context.getUrl());
      discovered.put(wwwRootUrl, urls);
    }
  }

  public void addVisited(UrlContext context) {
    if (context != null) {
      String wwwRootUrl = context.getOrigin().wwwRootUrl();
      Set<String> urls = visited.getOrDefault(wwwRootUrl, Sets.newHashSet());
      urls.add(context.getUrl());
      visited.put(wwwRootUrl, urls);
    }
  }

  public Set<String> getDiscovered(String wwwRootUrl) {
    if (StringUtils.isBlank(wwwRootUrl)) {
      throw new RuntimeException("Invalid www root url provided");
    }

    return discovered.getOrDefault(wwwRootUrl, Sets.newHashSet());
  }

  public Set<String> getVisited(String wwwRootUrl) {
    if (StringUtils.isBlank(wwwRootUrl)) {
      throw new RuntimeException("Invalid www root url provided");
    }

    return visited.getOrDefault(wwwRootUrl, Sets.newHashSet());
  }
}
