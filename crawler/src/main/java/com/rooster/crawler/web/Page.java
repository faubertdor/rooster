package com.rooster.crawler.web;

import com.google.common.base.Preconditions;

import java.util.Objects;

public class Page {
  private String url;
  private String title;
  private String textContent;

  public Page(String url, String title, String textContent) {
    Preconditions.checkNotNull(url, "url cannot be null");
    Preconditions.checkNotNull(title, "title cannot be null");
    Preconditions.checkNotNull(textContent, "text content cannot be null");

    this.url = url;
    this.title = title;
    this.textContent = textContent;
  }

  public String getUrl() {
    return url;
  }

  public String getTitle() {
    return title;
  }

  public String getTextContent() {
    return textContent;
  }

  @Override
  public boolean equals(Object object) {
    if (this == object) {
      return true;
    }

    if (!(object instanceof Page)) {
      return false;
    }

    Page webPage = (Page) object;
    return Objects.equals(url, webPage.url) &&
        Objects.equals(title, webPage.title) &&
        Objects.equals(textContent, webPage.textContent);
  }

  @Override
  public int hashCode() {
    return Objects.hash(url, title, textContent);
  }
}
