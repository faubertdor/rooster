package com.rooster.crawler.web.crawl;

import com.google.common.base.Preconditions;
import com.google.inject.Inject;

import org.apache.commons.lang3.StringUtils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import crawlercommons.robots.SimpleRobotRules;
import crawlercommons.robots.SimpleRobotRulesParser;

import java.net.URI;

public class RobotRulesParser {
  private static final Logger logger = LoggerFactory.getLogger(RobotRulesParser.class);
  public static final String TEXT_PLAIN = "text/plain";
  private URI crawlerUri;
  private String crawlerName;
  private SimpleRobotRulesParser parser;


  /**
   * Robot rules parser constructor
   *
   * @param crawlerUri URI for this crawler web page
   * @param crawlerName This crawler name
   * @param parser Simple robot rules parser
   */
  @Inject
  public RobotRulesParser(URI crawlerUri, String crawlerName, SimpleRobotRulesParser parser) {
    Preconditions.checkNotNull(crawlerUri, "crawler uri cannot be null");
    Preconditions.checkNotNull(crawlerName, "crawler name cannot be null");
    Preconditions.checkNotNull(parser, "robots rules parse cannot be null");

    this.crawlerUri = crawlerUri;
    this.crawlerName = crawlerName;
    this.parser = parser;
  }

  /**
   * Get the rules then parse the content
   *
   * @param robotstxt Website robots.txt content
   * @return Robot rules
   */
  public SimpleRobotRules parse(String robotstxt) {
    SimpleRobotRules rules = new SimpleRobotRules();

    if (StringUtils.isBlank(robotstxt)) {
      return rules;
    }

    try {
      rules = parser.parseContent(crawlerUri.toASCIIString(), robotstxt.getBytes(), TEXT_PLAIN, crawlerName);
    } catch (Exception e) {
      logger.error(String.format("Failed to parse robots.txt for %s - message: %s", robotstxt, e.getMessage()), e);
    }

    return rules;
  }
}
