package com.rooster.crawler.web;

import com.google.common.base.MoreObjects;
import com.google.common.base.Objects;
import com.google.common.base.Preconditions;

import crawlercommons.robots.SimpleRobotRules;

import java.net.URI;

public class Site {
  private static final String WWW = "www.";
  private String rootUrl;
  private String wwwRootUrl;
  private URI uri;
  private SimpleRobotRules rules;

  /**
   * Website URL to crawl and robots.txt rules that applies
   *
   * @param uri Root URI
   * @param rules Robots.txt rules
   */
  public Site(URI uri, SimpleRobotRules rules) {
    Preconditions.checkNotNull(uri, "URL cannot be null");
    Preconditions.checkNotNull(rules, "Robot rules cannot be null");

    String url = uri.toASCIIString();
    if (url.contains(WWW)) {
      this.wwwRootUrl = url;
      this.rootUrl = String.format("%s://%s", uri.getScheme(), uri.getAuthority().substring(WWW.length()));
    } else {
      this.rootUrl = url;
      this.wwwRootUrl = String.format("%s://%s%s", uri.getScheme(), WWW, uri.getAuthority());
    }
    this.uri = uri;
    this.rules = rules;
  }

  public URI getUri() {
    return uri;
  }

  public String getRootUrl() {
    return rootUrl;
  }

  public String wwwRootUrl() {
    return wwwRootUrl;
  }

  public SimpleRobotRules getRules() {
    return rules;
  }

  @Override
  public boolean equals(Object object) {
    if (this == object) {
      return true;
    }

    if (!(object instanceof Site)) {
      return false;
    }

    Site that = (Site) object;
    return Objects.equal(uri, that.uri) &&
        Objects.equal(rootUrl, that.rootUrl) &&
        Objects.equal(wwwRootUrl, that.wwwRootUrl) &&
        Objects.equal(rules, that.rules);
  }

  @Override
  public int hashCode() {
    return Objects.hashCode(uri, rules, rootUrl, wwwRootUrl);
  }

  @Override
  public String toString() {
    return MoreObjects.toStringHelper(Site.class)
        .add("uri", uri)
        .add("rootUrl", rootUrl)
        .add("wwwRootUrl", wwwRootUrl)
        .add("rules", rules)
        .toString();
  }
}