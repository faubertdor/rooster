package com.rooster.crawler.web;

import com.google.common.base.Preconditions;

import org.jsoup.select.Elements;

import java.util.Objects;

public class PageContext {
  private Site origin;
  private Elements links;

  public PageContext(Site origin, Elements links) {
    Preconditions.checkNotNull(origin, "site cannot be null");
    Preconditions.checkNotNull(links, "links cannot be null");

    this.origin = origin;
    this.links = links;
  }

  public Site getOrigin() {
    return origin;
  }

  public Elements getLinks() {
    return links;
  }

  @Override
  public boolean equals(Object object) {
    if (this == object) {
      return true;
    }

    if (!(object instanceof PageContext)) {
      return false;
    }

    PageContext that = (PageContext) object;
    return Objects.equals(origin, that.origin) &&
        Objects.equals(links, that.links);
  }

  @Override
  public int hashCode() {
    return Objects.hash(origin, links);
  }
}
