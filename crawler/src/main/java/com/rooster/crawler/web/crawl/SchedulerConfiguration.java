package com.rooster.crawler.web.crawl;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

import javax.validation.constraints.NotNull;

public class SchedulerConfiguration {

  @NotNull
  @JsonProperty
  private String crawlerName;

  @NotNull
  @JsonProperty
  private String crawlerUrl;

  @NotNull
  @JsonProperty
  private List<String> frontier;


  public String getCrawlerName() {
    return crawlerName;
  }

  public String getCrawlerUrl() {
    return crawlerUrl;
  }

  public List<String> getFrontier() {
    return frontier;
  }
}
