package com.rooster.core;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.yaml.YAMLFactory;

public class ObjectMapperFactory {
  private static ObjectMapper mapper = null;

  /**
   * Get an instance of object mapper
   *
   * @return ObjectMapper
   */
  public static ObjectMapper getInstance() {
    if (mapper == null) {
      mapper = new ObjectMapper(new YAMLFactory());
    }

    return mapper;
  }
}
