package com.rooster.core.config;

public interface ApplicationConfiguration {
  String getApplicationName();
}
