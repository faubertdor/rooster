package com.rooster.core;

import akka.japi.function.Function;
import akka.stream.Supervision;
import akka.stream.Supervision.Directive;

public class StreamSupervision implements Function<Throwable, Directive> {

  /**
   * Apply predefined supervision strategies for a given throwable
   *
   * @param param A given throwable
   * @return Directive
   * @throws Exception
   */
  @Override
  public Directive apply(Throwable param) throws Exception {
    return Supervision.resume();
  }
}
