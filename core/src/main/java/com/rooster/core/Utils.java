package com.rooster.core;

import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.File;
import java.io.IOException;

public class Utils {

  /**
   * Read a YAML config file given it's path into a class
   *
   * @param path File path
   * @param clazz Value type
   * @param <T> Generic
   * @return Generic
   */
  public static <T> T readYamlConfig(String path, Class<T> clazz) {
    ObjectMapper mapper = ObjectMapperFactory.getInstance();
    T config = null;

    try {
      config = mapper.readValue(new File(path), clazz);
    } catch (IOException e) {
      throw new RuntimeException(e);
    }

    return config;
  }
}
