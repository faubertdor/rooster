package com.rooster.core;

import akka.NotUsed;
import akka.stream.FlowShape;
import akka.stream.UniformFanInShape;
import akka.stream.UniformFanOutShape;
import akka.stream.javadsl.Balance;
import akka.stream.javadsl.Flow;
import akka.stream.javadsl.GraphDSL;
import akka.stream.javadsl.Merge;

@SuppressWarnings("unchecked")
public class BalancerFactory {

  /**
   * Create pool of worker for a given flow using balance and merge
   *
   * @param worker Worker flow
   * @param workerCount Parallelism
   * @param wait Wait for down streams
   * @param <In> Flow In
   * @param <Out> Flow Out
   * @return Flow
   */
  public static <In, Out> Flow<In, Out, NotUsed> balancer(Flow<In, Out, NotUsed> worker, int workerCount, boolean wait) {
    return Flow.fromGraph(
        GraphDSL.create(
            builder -> {
              final UniformFanOutShape<In, In> balance = builder.add(Balance.<In>create(workerCount, wait));
              final UniformFanInShape<Out, Out> merge = builder.add(Merge.<Out>create(workerCount));

              for (int i = 0; i < workerCount; i++) {
                builder.from(balance.out(i))
                    .via(builder.add(worker.async()))
                    .toInlet(merge.in(i));
              }

              return FlowShape.of(balance.in(), merge.out());
            }));
  }

  /**
   * Create pool of worker for a given flow using balance and merge with wait for down streams enabled
   *
   * @param worker Worker flow
   * @param workerCount Parallelism
   * @param <In> Flow In
   * @param <Out> Flow Out
   * @return Flow
   */
  public static <In, Out> Flow<In, Out, NotUsed> balancer(Flow<In, Out, NotUsed> worker, int workerCount) {
    return balancer(worker, workerCount, true);
  }
}
