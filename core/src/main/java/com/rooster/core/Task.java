package com.rooster.core;

import akka.stream.javadsl.Flow;

public interface Task<K, V> {

  V execute(K input);

  Flow flow(int parallelism);
}
